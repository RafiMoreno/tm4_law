from django.db import models

class NPM(models.Model):
    npm = models.CharField(max_length=50, unique=True)
    nama = models.CharField(max_length=100)

    def __str__(self):
        return self.npm
