from django.urls import path
from . import views
  
urlpatterns = [
    path('update/', views.update, name='update'),
    path('get/<str:npm>', views.get, name='get'),
]