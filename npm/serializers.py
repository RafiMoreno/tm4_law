from pyexpat import model
from rest_framework import serializers
from .models import NPM
from django.db.models import fields

class NPMSerializer(serializers.ModelSerializer):
    class Meta:
        model = NPM
        fields = "__all__"
