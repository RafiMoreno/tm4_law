from django.shortcuts import render
from npm.models import NPM
from npm.serializers import NPMSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import serializers
from rest_framework import status


@api_view(['POST'])
def update(request):
    npm = NPMSerializer(data = request.data)
    if NPM.objects.filter(**request.data).exists():
        raise serializers.ValidationError('This data already exists')
  
    if npm.is_valid():
        npm.save()
        return Response({
            'status': 'OK'
        })
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def get(request, npm):  
    try:
        npm_item = NPM.objects.get(npm=npm)
    except NPM.DoesNotExist:
        raise Response(status=status.HTTP_400_BAD_REQUEST)
    serializer = NPMSerializer(npm_item, many=False)
    if serializer:
        return Response({
            'status': 'OK',
            'npm': getattr(npm_item, 'npm'),
            'nama': getattr(npm_item, 'nama')
        })
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)


